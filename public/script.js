// Creating variables for the 'first operand', 'second operand', and the operation
let firstOperand, secondOperand, operation;

// Creating a variable and selecting the 'txt-input-display' element
const textInputDisplay = document.querySelector("#txt-input-display");

// Creating a variable and selecting all of the 'btn-numbers' elements
const buttonNumbers = document.querySelectorAll(".btn-numbers");

// Creating a variable and selecting the 'btn-add' element
const buttonAdd = document.querySelector("#btn-add");

// Creating a variable and selecting the 'btn-subtract' element
const buttonSubtract = document.querySelector("#btn-subtract");

// Creating a variable and selecting the 'btn-multiply' element
const buttonMultiply = document.querySelector("#btn-multiply");

// Creating a variable and selecting the 'btn-divide' element
const buttonDivide = document.querySelector("#btn-divide");

// Creating a variable and selecting the 'btn-equal' element
const buttonEqual = document.querySelector("#btn-equal");

// Creating a variable and selecting the 'btn-decimal' element
const buttonDecimal = document.querySelector("#btn-decimal");

// Creating a variable and selecting the 'btn-clear-all' element
const buttonClearAll = document.querySelector("#btn-clear-all");

// Creating a variable and selecting the 'btn-backspace' element
const buttonBackSpace = document.querySelector("#btn-backspace");

// Adding a 'click' event listener to all of the 'btn-numbers' by traversing them with 'forEach' statement
// the 'onclick' event retrieves 'textContent' property of the current button
// then it appends it to the current value of 'txt-input-display'

let textInputDisplayFloat; // parsing or converting string input to a float

buttonNumbers.forEach(buttonNumber => {
  buttonNumber.addEventListener('click', function(e){ 
    let currentText = e.target.textContent;
    
    // increment the value of 'textInputDisplay'
    textInputDisplay.value = textInputDisplay.value + currentText;
    textInputDisplayFloat = parseFloat(textInputDisplay.value);
  })
});


// Creating a 'click' event to add the two operands and display the result
buttonAdd.addEventListener('click', function(e) {
   firstOperand = textInputDisplayFloat;
   textInputDisplay.value = null;
   operation = e.target.innerText;
})

// Creating a 'click' event to subtract the two operands and display the result
buttonSubtract.addEventListener('click', function(e) {
   firstOperand = textInputDisplayFloat;
   textInputDisplay.value = null;
   operation = e.target.innerText;
})

// Creating a 'click' event to multiply the two operands and display the result
buttonMultiply.addEventListener('click', function(e) {
   firstOperand = textInputDisplayFloat;
   textInputDisplay.value = null;
   operation = e.target.innerText;
})

// Creating a 'click' event to divide the two operands and display the result
buttonDivide.addEventListener('click', function(e) {
   firstOperand = textInputDisplayFloat;
   textInputDisplay.value = null;
   operation = e.target.innerText;
})

// Creating a 'click' event to compute (equal) the result of the computation
buttonEqual.addEventListener('click', function(e) {
  let result;
  
   let secondOperandString = textInputDisplay.value;
   secondOperand = parseFloat(secondOperandString);
   if (operation === '+') {
       result = firstOperand + secondOperand;
   }
   else if (operation === '-') {
       result = firstOperand - secondOperand;
   }
   else if (operation === 'X') {
       result = firstOperand * secondOperand;
   }
   else if (operation === '/') {
       result = firstOperand / secondOperand;
   }
   textInputDisplay.value = result.toFixed(1);
})

// Creating a 'click' event to clear the input display
buttonClearAll.addEventListener('click', function(e){ 
    textInputDisplay.value =" ";
  });

// Creating a 'click' event to delete the last number from the operand
buttonBackSpace.addEventListener('click', function(e){ 
    textInputDisplay.value = textInputDisplay.value.slice(0, -1);
  });

// Creating a 'click' event to append a decimal character to the operand
buttonDecimal.addEventListener("click", e => {
  // Checking if '.' is already part of the text.value
  if (!textInputDisplay.value.includes('.')) {
    let currentText = e.target.textContent;
    textInputDisplay.value = textInputDisplay.value + currentText;
  }
});
